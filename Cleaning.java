import java.util.Date;

class Cleaning extends Employee {
    Cleaning(String nameBioskop, String addressBioskop, String openingHoursBioskop, String nameEmployee, Date startDate, int salary,
            int startWorkingHour, int endWorkingHour, String uniform, String[] cleaningGears) {
        super(nameBioskop, addressBioskop, openingHoursBioskop, nameEmployee, startDate, salary, startWorkingHour, endWorkingHour, uniform);
        this.cleaningGears = cleaningGears;
    }

    String[] cleaningGears;
}
