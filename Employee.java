import java.util.Date;

class Employee extends Bioskop{

    Employee(String nameBioskop, String addressBioskop, String openingHoursBioskop, String nameEmployee, Date startDate, int salary, int startWorkingHour, int endWorkingHour, String uniform) {
        super(nameBioskop, addressBioskop, openingHoursBioskop);
        this.name = nameEmployee;
        this.startDate = startDate;
        this.salary = salary;
        this.startWorkingHour = startWorkingHour;
        this.endWorkingHour = endWorkingHour;
        this.uniform = uniform;
    }
    String name;
    Date startDate;
    int salary;
    int startWorkingHour;
    int endWorkingHour;
    String uniform;
} 