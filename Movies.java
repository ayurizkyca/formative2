class Movies extends Studio {
    
    Movies(String nameBioskop, String addressBioskop, String openingHoursBioskop, String nameStudio, int seatsAmount, int price, String title) {
        super(nameBioskop, addressBioskop, openingHoursBioskop, nameStudio, seatsAmount);
        this.price = price;
        this.title = title;
    }
    int price;
    String title;
}
