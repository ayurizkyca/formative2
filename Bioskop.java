class Bioskop {
    String nameBioskop;
    String addressBioskop;
    String openingHoursBioskop;

    Bioskop(String nameBioskop,String addressBioskop, String openingHoursBioskop){
        this.nameBioskop = nameBioskop;
        this.addressBioskop = addressBioskop;
        this.openingHoursBioskop = openingHoursBioskop;
    }
}