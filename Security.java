import java.util.Date;

class Security extends Employee {
    Security(String nameBioskop, String addressBioskop, String openingHoursBioskop, String nameEmployee, Date startDate, int salary,
            int startWorkingHour, int endWorkingHour, String uniform, String[] certification,  String[] securityGears) {
        super(nameBioskop, addressBioskop, openingHoursBioskop, nameEmployee, startDate, salary, startWorkingHour, endWorkingHour, uniform);
        this.certification = certification;
        this.securityGears = securityGears;
    }
    String[] certification;
    String[] securityGears;
}
