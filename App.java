import java.util.Date;

public class App {

    public static void main(String[] args) {
        //DATA KASIR
        Cashier kasir = new Cashier("Metropole XXI", "Jl. Pegangsaan Tengah no. 21, Jakarta 10320 Indonesia", "24 Jam", "Jujun", new Date(), 300000, 8, 22, "Kasir", 900);
        System.out.println("\nKASIR");
        System.out.println("Nama Bioskop : " + kasir.nameBioskop);
        System.out.println("Alamat Bioskop : " + kasir.addressBioskop);
        System.out.println("Jam Buka : " + kasir.openingHoursBioskop);
        System.out.println("Nama : " + kasir.name);
        System.out.println("Gaji : " + kasir.salary);
        System.out.println("Tanggal Masuk : " + kasir.startDate);
        System.out.println("Jam Masuk : " + kasir.startWorkingHour);
        System.out.println("Jam Keluar : " + kasir.endWorkingHour);
        System.out.println("Uniform : " + kasir.uniform);
        System.out.println("Hasil Panjualan Tiket : " + kasir.hasilPenjualanTicket);
        
        //DATA SECURITY
        String[] securityCertification = {"Menangkap Maling", "Menangkap Penjahat"};
        String[] securityGears = {"Anjing", "Tali", "Pentung"};
        Security security = new Security("Metropole XXI", "Jl. Pegangsaan Tengah no. 21, Jakarta 10320 Indonesia", "24 Jam","Jojo", new Date(), 350000, 8, 22, "Satpam", securityCertification, securityGears );
        System.out.println("\nSECURITY");
        System.out.println("Nama Bioskop : " + security.nameBioskop);
        System.out.println("Alamat Bioskop : " + security.addressBioskop);
        System.out.println("Jam Buka : " + security.openingHoursBioskop);
        System.out.println("Nama : " + security.name);
        System.out.println("Gaji : " + security.salary);
        System.out.println("Tanggal Masuk : " + security.startDate);
        System.out.println("Jam Masuk : " + security.startWorkingHour);
        System.out.println("Jam Keluar : " + security.endWorkingHour);
        System.out.println("Uniform : " + security.uniform);
        System.out.println("Certification : " + String.join(", ", security.certification));
        System.out.println("Gear : " + String.join(", ", security.securityGears));

        //DATA CLEANING
        String[] cleaningGears = {"Pel", "Sapu"};
        Cleaning cleaning = new Cleaning("Metropole XXI", "Jl. Pegangsaan Tengah no. 21, Jakarta 10320 Indonesia", "24 Jam", "Zura", new Date(), 3000000, 8, 18, "Cleaning", cleaningGears);
        System.out.println("\nCLEANING");
        System.out.println("Nama Bioskop : " + cleaning.nameBioskop);
        System.out.println("Alamat Bioskop : " + cleaning.addressBioskop);
        System.out.println("Jam Buka : " + cleaning.openingHoursBioskop);
        System.out.println("Nama : " + cleaning.name);
        System.out.println("Gaji : " + cleaning.salary);
        System.out.println("Tanggal Masuk : " + cleaning.startDate);
        System.out.println("Jam Masuk : " + cleaning.startWorkingHour);
        System.out.println("Jam Keluar : " + cleaning.endWorkingHour);
        System.out.println("Uniform : " + cleaning.uniform);
        System.out.println("Gear : " + String.join(", ", cleaning.cleaningGears));

        //DATA STUDIO
        Studio studio = new Studio("Metropole XXI", "Jl. Pegangsaan Tengah no. 21, Jakarta 10320 Indonesia", "24 Jam", "Studio 1", 25);
        System.out.println("\nData Studio");
        System.out.println("Nama Bioskop : " + studio.nameBioskop);
        System.out.println("Alamat Bioskop : " + studio.addressBioskop);
        System.out.println("Jam Buka : " + studio.openingHoursBioskop);
        System.out.println("Nama Studio : " + studio.name);
        System.out.println("Jumlah Kursi : " + studio.seatsAmount);

        //DATA MOVIE
        Movies movie1 = new Movies("Metropole XXI", "Jl. Pegangsaan Tengah no. 21, Jakarta 10320 Indonesia", "24 Jam", "Studio 1", 25, 50000, "Marmut Merah Jambu");
        System.out.println("\nData Movie");
        System.out.println("Nama Bioskop : " + movie1.nameBioskop);
        System.out.println("Nama Studio : " + movie1.name);
        System.out.println("Jumlah Kursi : " + movie1.seatsAmount);
        System.out.println("Harga Tiket : " + movie1.price);
        System.out.println("Judul Film : " + movie1.title);

        Movies movie2 = new Movies("Metropole XXI", "Jl. Pegangsaan Tengah no. 21, Jakarta 10320 Indonesia", "24 Jam", "Studio 2", 10, 600000, "Kuda Terbang");
        System.out.println("\nData Movie 2");
        System.out.println("Nama Bioskop : " + movie2.nameBioskop);
        System.out.println("Nama Studio : " + movie2.name);
        System.out.println("Jumlah Kursi : " + movie2.seatsAmount);
        System.out.println("Harga Tiket : " + movie2.price);
        System.out.println("Judul Film : " + movie2.title);
    }
}
