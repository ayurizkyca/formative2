class Studio extends Bioskop {
    
    Studio(String nameBioskop, String addressBioskop, String openingHoursBioskop, String nameStudio, int seatsAmount ) {
        super(nameBioskop, addressBioskop, openingHoursBioskop);
        this.name = nameStudio;
        this.seatsAmount = seatsAmount;
    }
    String name;
    int seatsAmount;
}
